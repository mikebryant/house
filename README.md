# house

My house

## Bootstrapping onto a Kubernetes cluster

1. kubectl create namespace argocd
1. helm repo add argo https://argoproj.github.io/argo-helm
1. helm install --namespace argocd argocd argo/argo-cd --version 5.19.11
1. kubectl apply -f bootstrap/application.yaml
